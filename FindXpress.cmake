#adapted from SCIPOPT
find_path(XPRESS_INCLUDE_DIRS
    xprs.h
    HINTS ${XPRESS_DIR} $ENV{XPRESSDIR}
    PATH_SUFFIXES include)

# todo: enable recursive search
find_library(XPRESS_LIBRARY
    NAMES xprs
    HINTS ${XPRESS_DIR} $ENV{XPRESSDIR}
    PATH_SUFFIXES lib)


# todo properly check when pthread is necessary
set(XPRESS_LIBRARIES ${XPRESS_LIBRARY})

message(STATUS "XPRESS C++ library: ${XPRESS_LIBRARIES}")
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Xpress DEFAULT_MSG
    XPRESS_INCLUDE_DIRS)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Xpress DEFAULT_MSG XPRESS_LIBRARY)
#FIND_PACKAGE_HANDLE_STANDARD_ARGS(Xpress DEFAULT_MSG XPRESS_LINK_DIR)
mark_as_advanced(XPRESS_INCLUDE_DIRS)
mark_as_advanced(XPRESS_LIBRARY)
