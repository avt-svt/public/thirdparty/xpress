This is a wrapper for XPRESS.
It requires a separate XPRES installation and merely provides the CMakeLists.txt and FindXpress.cmake for including it in other code.
The pre-processor flag HAVE_XPRESS can be used in the client code to check.
